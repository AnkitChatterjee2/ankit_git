﻿using System.Web;
using System.Web.Mvc;

namespace viewBag_viewdata_Tempdata
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
