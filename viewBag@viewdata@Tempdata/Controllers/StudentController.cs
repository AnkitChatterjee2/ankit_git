﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using viewBag_viewdata_Tempdata.Models;

namespace viewBag_viewdata_Tempdata.Controllers
{
    public class StudentController : Controller
    {
        
        // GET: Student
        public ActionResult Index()
        {
            ViewData["Student"] = "Nitin Tyagi";


            ViewBag.Name = "Nitin Tyagi";

            TempData["Name"] = "Nitin Tyagi";

            return RedirectToAction("Index1");
        }


        //summary
          //Viewresult example
        //Summary
        public ViewResult Index1()
        {

            ViewBag.data = TempData["Name"].ToString();

            return View();
        }

             

            /// <summary>  
            /// Welcome Note Message  
            /// </summary>  
            /// <returns>In a Json Format</returns>  
            public JsonResult WelcomeNote()
            {
                bool isAdmin = false;
                //TODO: Check the user if it is admin or normal user, (true-Admin, false- Normal user)  
                string output = isAdmin ? "Welcome to the Admin User" : "Welcome to the User";

                return Json(output, JsonRequestBehavior.AllowGet);
            }


        //Summary
         //Example of file result sdasd
         //Summary
         public FileResult DownloadFile()
        {
            return new FilePathResult("~/File/File.docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");

        }
        


    }
}