﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace viewBag_viewdata_Tempdata.Models
{
    
        public class Student
        {
            public int StudentId { get; set; }
            public string StudentName { get; set; }
            public string Address { get; set; }
        }
    
}